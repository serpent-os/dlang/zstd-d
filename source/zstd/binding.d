module zstd.binding;

extern (C):
@nogc:
nothrow:

uint ZSTD_versionNumber();

const(char)* ZSTD_versionString();

size_t ZSTD_compress(void* dst, size_t dstCapacity, const(void)* src,
        size_t srcSize, int compressionLevel);

size_t ZSTD_decompress(void* dst, size_t dstCapacity, const(void)* src, size_t compressedSize);

ulong ZSTD_getFrameContentSize(const(void)* src, size_t srcSize);

ulong ZSTD_getDecompressedSize(const(void)* src, size_t srcSize);

size_t ZSTD_findFrameCompressedSize(const(void)* src, size_t srcSize);

size_t ZSTD_compressBound(size_t srcSize);
uint ZSTD_isError(size_t code);
const(char)* ZSTD_getErrorName(size_t code);
int ZSTD_minCLevel();
int ZSTD_maxCLevel();
int ZSTD_defaultCLevel();

struct ZSTD_CCtx_s;
alias ZSTD_CCtx = ZSTD_CCtx_s;
ZSTD_CCtx* ZSTD_createCCtx();
size_t ZSTD_freeCCtx(ZSTD_CCtx* cctx);

size_t ZSTD_compressCCtx(ZSTD_CCtx* cctx, void* dst, size_t dstCapacity,
        const(void)* src, size_t srcSize, int compressionLevel);

struct ZSTD_DCtx_s;
alias ZSTD_DCtx = ZSTD_DCtx_s;
ZSTD_DCtx* ZSTD_createDCtx();
size_t ZSTD_freeDCtx(ZSTD_DCtx* dctx);

size_t ZSTD_decompressDCtx(ZSTD_DCtx* dctx, void* dst, size_t dstCapacity,
        const(void)* src, size_t srcSize);

enum ZSTD_strategy
{
    fast = 1,
    dfast = 2,
    greedy = 3,
    lazy_ = 4,
    lazy2 = 5,
    btlazy2 = 6,
    btopt = 7,
    btultra = 8,
    btultra2 = 9
}

enum ZSTD_cParameter
{
    compressionLevel = 100,

    windowLog = 101,

    hashLog = 102,

    chainLog = 103,

    searchLog = 104,

    minMatch = 105,

    targetLength = 106,

    strategy = 107,

    enableLongDistanceMatching = 160,

    ldmHashLog = 161,

    ldmMinMatch = 162,

    ldmBucketSizeLog = 163,

    ldmHashRateLog = 164,

    contentSizeFlag = 200,

    checksumFlag = 201,
    dictIDFlag = 202,

    nbWorkers = 400,

    jobSize = 401,

    overlapLog = 402,

    experimentalParam1 = 500,
    experimentalParam2 = 10,
    experimentalParam3 = 1000,
    experimentalParam4 = 1001,
    experimentalParam5 = 1002,
    experimentalParam6 = 1003,
    experimentalParam7 = 1004,
    experimentalParam8 = 1005,
    experimentalParam9 = 1006,
    experimentalParam10 = 1007,
    experimentalParam11 = 1008,
    experimentalParam12 = 1009,
    experimentalParam13 = 1010,
    experimentalParam14 = 1011,
    experimentalParam15 = 1012
}

struct ZSTD_bounds
{
    size_t error;
    int lowerBound;
    int upperBound;
}

ZSTD_bounds ZSTD_cParam_getBounds(ZSTD_cParameter cParam);

size_t ZSTD_CCtx_setParameter(ZSTD_CCtx* cctx, ZSTD_cParameter param, int value);

size_t ZSTD_CCtx_setPledgedSrcSize(ZSTD_CCtx* cctx, ulong pledgedSrcSize);

enum ZSTD_ResetDirective
{
    sessionOnly = 1,
    parameters = 2,
    sessionAndParameters = 3
}

size_t ZSTD_CCtx_reset(ZSTD_CCtx* cctx, ZSTD_ResetDirective reset);

size_t ZSTD_compress2(ZSTD_CCtx* cctx, void* dst, size_t dstCapacity,
        const(void)* src, size_t srcSize);

enum ZSTD_dParameter
{
    windowLogMax = 100,

    experimentalParam1 = 1000,
    experimentalParam2 = 1001,
    experimentalParam3 = 1002,
    experimentalParam4 = 1003
}

ZSTD_bounds ZSTD_dParam_getBounds(ZSTD_dParameter dParam);

size_t ZSTD_DCtx_setParameter(ZSTD_DCtx* dctx, ZSTD_dParameter param, int value);

size_t ZSTD_DCtx_reset(ZSTD_DCtx* dctx, ZSTD_ResetDirective reset);

struct ZSTD_inBuffer_s
{
    const(void)* src;
    size_t size;
    size_t pos;
}

alias ZSTD_inBuffer = ZSTD_inBuffer_s;

struct ZSTD_outBuffer_s
{
    void* dst;
    size_t size;
    size_t pos;
}

alias ZSTD_outBuffer = ZSTD_outBuffer_s;

alias ZSTD_CStream = ZSTD_CCtx_s;

ZSTD_CStream* ZSTD_createCStream();
size_t ZSTD_freeCStream(ZSTD_CStream* zcs);

enum ZSTD_EndDirective
{
    continue_ = 0,
    flush = 1,

    end = 2
}

size_t ZSTD_compressStream2(ZSTD_CCtx* cctx, ZSTD_outBuffer* output,
        ZSTD_inBuffer* input, ZSTD_EndDirective endOp);

size_t ZSTD_CStreamInSize();
size_t ZSTD_CStreamOutSize();

size_t ZSTD_initCStream(ZSTD_CStream* zcs, int compressionLevel);

size_t ZSTD_compressStream(ZSTD_CStream* zcs, ZSTD_outBuffer* output, ZSTD_inBuffer* input);

size_t ZSTD_flushStream(ZSTD_CStream* zcs, ZSTD_outBuffer* output);

size_t ZSTD_endStream(ZSTD_CStream* zcs, ZSTD_outBuffer* output);

alias ZSTD_DStream = ZSTD_DCtx_s;

ZSTD_DStream* ZSTD_createDStream();
size_t ZSTD_freeDStream(ZSTD_DStream* zds);

size_t ZSTD_initDStream(ZSTD_DStream* zds);

size_t ZSTD_decompressStream(ZSTD_DStream* zds, ZSTD_outBuffer* output, ZSTD_inBuffer* input);

size_t ZSTD_DStreamInSize();
size_t ZSTD_DStreamOutSize();

size_t ZSTD_compress_usingDict(ZSTD_CCtx* ctx, void* dst, size_t dstCapacity,
        const(void)* src, size_t srcSize, const(void)* dict, size_t dictSize, int compressionLevel);

size_t ZSTD_decompress_usingDict(ZSTD_DCtx* dctx, void* dst, size_t dstCapacity,
        const(void)* src, size_t srcSize, const(void)* dict, size_t dictSize);

struct ZSTD_CDict_s;
alias ZSTD_CDict = ZSTD_CDict_s;

ZSTD_CDict* ZSTD_createCDict(const(void)* dictBuffer, size_t dictSize, int compressionLevel);

size_t ZSTD_freeCDict(ZSTD_CDict* CDict);

size_t ZSTD_compress_usingCDict(ZSTD_CCtx* cctx, void* dst, size_t dstCapacity,
        const(void)* src, size_t srcSize, const(ZSTD_CDict)* cdict);

struct ZSTD_DDict_s;
alias ZSTD_DDict = ZSTD_DDict_s;

ZSTD_DDict* ZSTD_createDDict(const(void)* dictBuffer, size_t dictSize);

size_t ZSTD_freeDDict(ZSTD_DDict* ddict);

size_t ZSTD_decompress_usingDDict(ZSTD_DCtx* dctx, void* dst,
        size_t dstCapacity, const(void)* src, size_t srcSize, const(ZSTD_DDict)* ddict);

uint ZSTD_getDictID_fromDict(const(void)* dict, size_t dictSize);

uint ZSTD_getDictID_fromCDict(const(ZSTD_CDict)* cdict);

uint ZSTD_getDictID_fromDDict(const(ZSTD_DDict)* ddict);

uint ZSTD_getDictID_fromFrame(const(void)* src, size_t srcSize);

size_t ZSTD_CCtx_loadDictionary(ZSTD_CCtx* cctx, const(void)* dict, size_t dictSize);

size_t ZSTD_CCtx_refCDict(ZSTD_CCtx* cctx, const(ZSTD_CDict)* cdict);

size_t ZSTD_CCtx_refPrefix(ZSTD_CCtx* cctx, const(void)* prefix, size_t prefixSize);

size_t ZSTD_DCtx_loadDictionary(ZSTD_DCtx* dctx, const(void)* dict, size_t dictSize);

size_t ZSTD_DCtx_refDDict(ZSTD_DCtx* dctx, const(ZSTD_DDict)* ddict);

size_t ZSTD_DCtx_refPrefix(ZSTD_DCtx* dctx, const(void)* prefix, size_t prefixSize);

size_t ZSTD_sizeof_CCtx(const(ZSTD_CCtx)* cctx);
size_t ZSTD_sizeof_DCtx(const(ZSTD_DCtx)* dctx);
size_t ZSTD_sizeof_CStream(const(ZSTD_CStream)* zcs);
size_t ZSTD_sizeof_DStream(const(ZSTD_DStream)* zds);
size_t ZSTD_sizeof_CDict(const(ZSTD_CDict)* cdict);
size_t ZSTD_sizeof_DDict(const(ZSTD_DDict)* ddict);
