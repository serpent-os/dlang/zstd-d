#!/bin/bash
set -e
set -x

dstep -o binding.d \
        /usr/include/zstd.h \
        --rename-enum-members=true \
        --package zstd \
        --comments=false \
        --translate-macros=false \
        --global-attribute '@nogc' \
        --global-attribute 'nothrow'

cp binding.d source/zstd/binding.d
rm binding.d
