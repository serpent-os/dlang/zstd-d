### zstd-d

A direct binding to the zstd library

Will eventually be used for all zstd operations in moss/boulder/etc
to drop reliance on multiple dead forks of projects.
